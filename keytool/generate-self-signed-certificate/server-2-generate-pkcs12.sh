#!/bin/bash

# Generates a key pair (a public key and associated private key). 
# Wraps the public key into an X.509 v3 self-signed certificate, which is stored as a single-element certificate chain. 
# This certificate chain and the private key are stored in a new keystore entry identified by alias.

#pass: server123456

keytool \
  -genkeypair \
  -dname "CN=server.my.com, O=My Company, C=PL" \
  -alias my-server2 \
  -keyalg RSA \
  -keysize 2048 \
  -storetype PKCS12 \
  -keystore my-server2.p12 \
  -validity 3650 \
  -storepass server123456

