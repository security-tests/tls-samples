#!/bin/bash

#pass: trust123456

keytool -importcert -storetype PKCS12 -keystore server-truststore.p12 \
  -storepass trust123456 -alias client-ca -file my-client.crt -noprompt
