# Prepare key and trust sotores for two way TLS

## 1. Generate key pair for the server nad client

```
keytool -genkeypair \
  -dname "CN=server.my.com, O=My Company, C=PL" \
  -alias my-server \
  -keyalg RSA \
  -keysize 2048 \
  -storetype PKCS12 \
  -keystore my-server.p12 \
  -validity 3650 \
  -storepass server123456
```
```
keytool -genkeypair \
  -dname "CN=client.my.com, O=My Company, C=PL" \
  -alias my-client \
  -keyalg RSA \
  -keysize 2048 \
  -storetype PKCS12 \
  -keystore my-client.p12 \
  -validity 3650 \
  -storepass client123456
```

## 2. List keystore entries

```
keytool -list \
  -keystore my-server.p12 \
  -storepass server123456
``` 

## 3. Export certificate from keystore

```
keytool -exportcert \
  -file my-server.crt \
  -keystore my-server.p12 \
  -alias my-server \
  -storepass server123456
```

```
keytool -exportcert \
  -file my-client.crt \
  -keystore my-client.p12 \
  -alias my-client \
  -storepass client123456
```

## 4. Print cerificate details

```
keytool -printcert -file my-server.crt
```

## 5. Create trust store

```
keytool -importcert \
  -storetype PKCS12 \
  -keystore server-truststore.p12 \
  -alias client-ca \
  -storepass trust123456 \
  -file my-client.crt \
  -noprompt
```

```
keytool -importcert \
  -storetype PKCS12 \
  -keystore client-truststore.p12 \
  -alias server-ca \
  -storepass trust123456 \
  -file my-server.crt \
  -noprompt
```

## 6. List truststore entries

```
keytool -list \
  -keystore client-truststore.p12 \
  -storepass trust123456
``` 