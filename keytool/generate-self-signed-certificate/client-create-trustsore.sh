#!/bin/bash

#pass: trust123456

keytool -importcert -storetype PKCS12 -keystore client-truststore.p12 \
  -storepass trust123456 -alias server-ca -file my-server.crt -noprompt
