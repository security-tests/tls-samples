#!/bin/bash

#pass: trust123456

keytool -importcert -storetype PKCS12 -keystore client-truststore-2-servers.p12 \
  -storepass trust123456 -alias server1-ca -file my-server.crt -noprompt

keytool -importcert -storetype PKCS12 -keystore client-truststore-2-servers.p12 \
  -storepass trust123456 -alias server2-ca -file my-server2.crt -noprompt
