package org.gol.tlsserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TlsServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(TlsServerApplication.class, args);
    }

}
