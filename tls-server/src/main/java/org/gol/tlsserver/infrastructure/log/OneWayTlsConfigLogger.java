package org.gol.tlsserver.infrastructure.log;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;

import javax.annotation.PostConstruct;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Profile("one-way-tls")
class OneWayTlsConfigLogger {

    @Value("${server.ssl.key-store:}")
    private Resource keyStore;

    @PostConstruct
    void log() throws IOException {
        log.info("One way TLS configured.");
        log.info("Using keystore: {}", keyStore.getURL());
    }
}
