package org.gol.tlsserver.infrastructure.log;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;

import javax.annotation.PostConstruct;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Profile("two-way-tls")
class TwoWayTlsConfigLogger {

    @Value("${server.ssl.key-store:}")
    private Resource keyStore;

    @Value("${server.ssl.trust-store:}")
    private Resource trustStore;

    @PostConstruct
    void log() throws IOException {
        log.info("Two way TLS configured.");
        log.info("Using keystore: {}", keyStore.getURL());
        log.info("Using truststore: {}", trustStore.getURL());
    }
}
