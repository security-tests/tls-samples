package org.gol.tlsserver.application;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class RootResponse {
    private final String secure;
}
