package org.gol.tlsserver.application;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/")
public class RootAdapter {

    @GetMapping()
    public ResponseEntity<RootResponse> root() {
        return ok(new RootResponse(UUID.randomUUID().toString()));
    }
}
