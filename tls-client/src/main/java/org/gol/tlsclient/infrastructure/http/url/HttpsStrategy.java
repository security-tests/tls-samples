package org.gol.tlsclient.infrastructure.http.url;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("!no-tls")
class HttpsStrategy implements UrlCreator {

    private static final String SCHEMA = "https://";

    @Override
    public String prepareUrl(String endpoint) {
        return SCHEMA + endpoint;
    }
}
