package org.gol.tlsclient.infrastructure.http.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@Profile("no-tls")
class NoTlsConfig {

    @Bean
    RestTemplate restTemplate() {
        log.info("No TLS configured.");
        return new RestTemplate(new HttpComponentsClientHttpRequestFactory());
    }
}
