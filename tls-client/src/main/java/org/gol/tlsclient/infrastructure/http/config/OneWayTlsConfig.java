package org.gol.tlsclient.infrastructure.http.config;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@Profile("one-way-tls")
class OneWayTlsConfig {

    @Value("${trust-store.location}")
    private Resource trustStore;

    @Value("${trust-store.password}")
    private String trustStorePass;

    @Bean
    RestTemplate restTemplate() throws CertificateException, NoSuchAlgorithmException, KeyStoreException,
            IOException, KeyManagementException {
        log.info("One way TLS configured.");
        log.info("Using truststore: {}", trustStore.getURL());
        var sslContext = new SSLContextBuilder()
                .loadTrustMaterial(trustStore.getURL(), trustStorePass.toCharArray())
                .build();
        var httpClient = HttpClients.custom()
                .setSSLSocketFactory(new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE))
                .build();
        return new RestTemplate(new HttpComponentsClientHttpRequestFactory(httpClient));
    }
}
