package org.gol.tlsclient.infrastructure.http.url;

public interface UrlCreator {
    String prepareUrl(String endpoint);
}
