package org.gol.tlsclient.application;

import org.gol.tlsclient.infrastructure.http.url.UrlCreator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import io.vavr.control.Try;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import static java.util.Arrays.stream;

@Slf4j
@Service
@RequiredArgsConstructor
public class ConnectorAdapter {

    private final UrlCreator urlCreator;
    private final @NonNull RestTemplate restTemplate;

    @Value("${tls-server.endpoint}")
    private String[] endpoints;


    @Scheduled(fixedRate = 5000)
    private void callServer() {
        stream(endpoints)
                .distinct()
                .map(urlCreator::prepareUrl)
                .forEach(this::callServer);
    }

    private void callServer(String url) {
        Try.of(() -> restTemplate.getForObject(url, ServerResponse.class))
                .onSuccess(resp -> log.debug("GET {}: {}", url, resp))
                .onFailure(e -> log.error("GET {}: {}", url, e.getMessage()));
    }
}
