package org.gol.tlsclient.application;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@NoArgsConstructor
class ServerResponse {
    private String secure;
}
